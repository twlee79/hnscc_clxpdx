#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Contains generic Snakemake rules and associated functions for preparing
input sequencing data for subsequent processing.
"""
# -------------------------------------------------------------------------------
# Author:       Tet Woo Lee
#
# Created:      2019-01-17
# Copyright:    (c) Tet Woo Lee 2019
# Licence:      GPLv3
#
# Dependencies: path_templater (https://pypi.org/project/pathtemplater/)
#
# Change log:
#
# v1.0.0.dev8 2019-06-22
# : Fixed bug in has_end2
#
# v1.0.0.dev7 2019-02-18
# : Added get_all_inputs(), now needed as sample_name is store as list in
#   inputid_dict
#
# v1.0.0.dev6 2019-02-13
# : Added has_end2()
# : Improved sanity checks for unexpected conditions when building input file
#   list
#
# v1.0.0.dev5 2019-02-13
# : Fix nasty bug causing only L001 to be used
#
# v1.0.0.dev4 2019-02-12
# : Add inputid wildcard_constraints
#
# v1.0.0.dev3 2019-02-12
# : Added allow_multiple_samples_per_input
#
# v1.0.0.dev2 2019-02-08
# : Updated for PathTemplater v1.0.0.dev7
#
# v1.0.0.dev1 2019-02-07
# : Complete version, tested and working with PathTemplater v1.0.0.dev5
#
# v0.1
# : First working version
# -------------------------------------------------------------------------------


# sampling and staging input
import pathlib
import re
import pandas
import pprint
import string
import copy
from pathtemplater import PathTemplater

print("Including prepare_input.smk...")

### LOAD CONFIG PARAMETERS
# helper to load a parameter from config file into global namespace
def load_config_parameter_default(param_name):
    globals()[param_name] = config[param_name]

try: load_config_parameter # for overriding
except: load_config_parameter = load_config_parameter_default
load_config_parameter('verbose')

load_config_parameter('input_root_directory')
load_config_parameter('input_extensions')
load_config_parameter('input_end1_suffix')
load_config_parameter('input_end2_suffix')
load_config_parameter('ignore_valid_extension_uncollected')
load_config_parameter('sample_list_path')


load_config_parameter('paired_reads')
load_config_parameter('ignore_missing_end2')
load_config_parameter('allow_multiple_samples_per_input')

load_config_parameter('subsample_input')

load_config_parameter('staged_input_directory')
load_config_parameter('staged_input_extension')

load_config_parameter('subsampled_input_directory')
load_config_parameter('subsampled_input_extension')

load_config_parameter('combined_input_directory')
load_config_parameter('combined_input_extension')

load_config_parameter('end1_label')
load_config_parameter('end2_label')

load_config_parameter('path_templater_top_directories')
load_config_parameter('path_templater_alt_extensions')

### HELPER FUNCTIONS AND CLASSES
_pi_pp = pprint.PrettyPrinter(indent=4)

def warn_user_default(message):
    """
    Output a warning message to the user.
    """
    if verbose: print("Warning:",message, file=sys.stderr)
    # using Warning.warn in snakemake shows warning in wrong code location e.g.
    # /home/user/anaconda3/envs/snakemake/lib/python3.6/site-packages/snakemake/workflow.py:318: UserWarning: warning message
    #   return map(self._rules.__getitem__, filter(self.is_rule, items))
    # so better to just use simply message printing

try: warn_user
except: warn_user = warn_user_default

# print input,output,log and param of all rules
# suggest adding if verbose: print_all_rules() to end of main Snakefile
# this uses undocumented features of snakemake, so may fail with new versions
# tested with Snakemake 5.10
def print_all_rules_default():
    print("List of all rules:")
    for rule_name in vars(rules):
        print("rule {}:".format(rule_name))
        rule_obj = getattr(rules, rule_name)
        def expand_attribute(attr):
            if hasattr(attr,"allitems"):
                allitems = attr.allitems()
            elif hasattr(attr,"_allitems"):
                allitems = attr._allitems()
            else:
                raise Exception("Unable to access allitems from snakemake object")
            formatted_items = []
            for i,item in enumerate(allitems):
                item_name,item_value = item
                formatted = ""
                if item_name is not None:
                    formatted = "{} = ".format(item_name)
                formatted+=str(item_value)
                formatted_items.append(formatted)
            return formatted_items

        for attr_name in ('input','output','log','params'):
            attr_obj = getattr(rule_obj.rule,attr_name)
            expanded = expand_attribute(attr_obj)
            if expanded:
                print("  {}:\n    {}".format(attr_name,"\n    ".join(expanded)))
        print()

try: print_all_rules
except: print_all_rules = print_all_rules_default


### TEMPLATES FOR FORMATTING PATHS

## Standard naming of inputs

# standard format of inputid
standard_inputid = "{run_id}__{input_basefilename}"
inputid_template = "{inputid}"
wildcard_constraints: # based on above standard_inputid must contain two underscores
    inputid = ".+__.+"

# standard naming of end labels
end_labels = [end1_label, end2_label]
    # can be used as iterable to expand both ends
end1_2_label = "-".join(end_labels)
    # label to denote both ends
end_label_ = "end_label"
    # wildcard name within 'end_label' wildcard
    # Warning: care has been taken to ensure all code will still work if this
    # variable is changed, but this has not been exhaustively tested

end_label_wildcard = "{"+end_label_+"}"
inputid_endx_template = "{inputid}_"+end_label_wildcard  # inputid for end x

# templates for expanding all inputids
inputid_expand_template = "{input[inputid]}"
    # unusual, but designed to allow to be compatible with sample_inputid_expand_template
inputid_endx_expand_template = inputid_expand_template+"_"+end_label_wildcard
    # as above for end x

# standard naming with sample_name included
sample_inputid_template = "{sample_name}___{inputid}"
    # inputid plus sample_name
sample_inputid_endx_template = sample_inputid_template+"_"+end_label_wildcard
    # as above for end x

# templates for expanding all inputids including sample_name
sample_inputid_expand_template = "{input[sample_name]}___{input[inputid]}"
    # use as template for expanding all inputids together with their associated
    # sample names by providing a iterable of input dicts containing both
    # sample_name and inputid
sample_inputid_endx_expand_template = sample_inputid_expand_template+"_"+end_label_wildcard
    # as above for end x

# templates for expanding all samples
sample_combined_template = "{sample_name}___all"
    # use as a template for expanding all samples using a sample_name iterable,
sample_combined_endx_template = sample_combined_template+"_"+end_label_wildcard
    # as above for end x

# automatic selection of source templates
# if multiple samples per input, only use inputid (as sample_name isn't unique per inputid)
# if only one sample per input, include sample_name
if allow_multiple_samples_per_input:
    source_endx_template = inputid_endx_template
    source_endx_expand_template = inputid_endx_expand_template
else:
    source_endx_template = sample_inputid_endx_template
    source_endx_expand_template = sample_inputid_endx_expand_template

## Automatic selection of staged or subsampled inputs based on subsample param
# see templates below for simple selection of input sources
if subsample_input:
    print("Inputs will be subsampled...")
    input_source_directory = subsampled_input_directory
    input_source_extension = subsampled_input_extension
else:
    input_source_directory = staged_input_directory
    input_source_extension = staged_input_extension

# Templates for correctly selecting input sources

# create a PathTemplater type allowing various top directories,
# log and complete alternate suffixes, and supporting easy end_label
# formatting expansion
# print(path_templater_directories)
# {'input': 'input', 'intermediate': 'interm', 'output': 'output', 'log': 'logs'}
# print(path_templater_extensions)
# {'log': '.log', 'complete': '+.complete'}
path_templater_preset_formats = { 'end1': {end_label_: end1_label},
                                  'end2': {end_label_: end2_label},
                                  'end1_2': {end_label_: end1_2_label},
                                  'expand_ends': {end_label_: end_labels},
                                 }
path_templater = PathTemplater(path_templater_top_directories,
                               path_templater_alt_extensions,
                               path_templater_preset_formats).intermediatedir()
                               # initalize to intermediate directory

input_source_template = path_templater.create_fromparts(input_source_directory,
    source_endx_template,input_source_extension)
    # standard input source templater, selects staged or subsample inputs
    # and contains sample_name, inputid, and end_label wildcards
combined_input_source_template = path_templater.create_fromparts(
    combined_input_directory, sample_combined_endx_template, combined_input_extension)
    # combined input templater with sample_name and end_label wildcards


# expands to produce list of all input sourcees
def expand_all_input_sources_default():
    return path_templater.create_fromparts(input_source_directory,
        source_endx_expand_template,
        input_source_extension).expand(input=inputid_dict.values(),
        end_label=end_labels)

try: expand_all_input_sources
except: expand_all_input_sources = expand_all_input_sources_default

def expand_all_combined_input_sources_default():
    return combined_input_source_template.expand(sample_name = sample_dict.keys(),
        end_label=end_labels)

try: expand_all_combined_input_sources
except: expand_all_combined_input_sources = expand_all_combined_input_sources_default


# read sample list returning as pandas df
def read_sample_list_default():
    print("Reading sample list...")
    if sample_list_path.endswith('.csv'): delim = ','
    else: delim = '\t' # default to tsv
    sample_df = pandas.read_table(sample_list_path, delim).sort_values(
        by=['sample_name'])
    print(sample_df.to_string())
    return sample_df

# requires global options:
# input_root_directory: toplevel directory to start walking
# input_end1_suffix, input_end2_suffix: suffixes to identify read end1 and end2 files
# input_extensions: tuple of extensions used to identify input fastq files
# verbose: boolean of whether or not to produce verbose output
# paired_reads: boolean of whether to expect paired reads
# ignore_missing_end2: boolean of whether to ignore missing end2 reads when paired reads used
#
# expects filepaths to be in the format "{run_id}/.*/.*{suffix}{extension}$" within
# input_root_directory
#
# returns a pandas df containing columns: run_id,input_basefilename,inputid,end1_path,(end2_path)
# the df is sanity-checked for missing files based on above parameters

# this is the default version of build_inputid_list
# function can be overriden by defining build_inputid_list prior to including this file
# any version should produce a pandas df containing the above columns

def build_inputid_list_default():
    print("Finding all fastq (paired) read files...")
    input_file_collection = {}
        # stores collection of input_files as inputid:details dict
    # build regex pattern for finding input files
    input_file_pattern = "^(?P<input_basefilename>.*)(?P<suffix>{})(?P<extension>{})$".format(
        '|'.join([re.escape(input_end1_suffix),re.escape(input_end2_suffix)]),
        '|'.join([re.escape(extension) for extension in input_extensions])
    )
    if verbose: print(" Identifying input files using regex pattern:",input_file_pattern)
    # walk dir tree looking for files with specified extensions
    input_root_path = pathlib.Path(input_root_directory)
    for walk_root, dirnames, filenames in os.walk(input_root_path):
        walk_root_path = pathlib.Path(walk_root)
        for filename in filenames:
            filepath = walk_root_path / filename
            filepath_str = str(filepath)
            if verbose: print("  Found file:",filepath_str)
            # does filename match regex?
            filename_match = re.match(input_file_pattern,filename)
            if filename_match:
                # yes
                filename_groups = filename_match.groupdict()
                inputfile_details = {}
                inputfile_details['run_id'] = filepath.relative_to(input_root_path).parts[0]
                    # run_id = first dir part after base directory
                inputfile_details['input_basefilename'] = filename_groups['input_basefilename']
                inputid = standard_inputid.format(
                    **inputfile_details
                ) # derive inputname from run_id and basename
                inputfile_details['inputid'] = inputid
                # obtain input details dict from full input collection if present
                # (we may be adding file for other end to the details)
                inputfile_details = input_file_collection.setdefault(
                    inputid, inputfile_details
                )
                # which end is this?
                end = 'end1' if filename_groups['suffix']==input_end1_suffix else 'end2'
                if verbose: print("   File is for read",end)
                end_path_key=end+'_path'
                # add endx_path to details
                if end_path_key in inputfile_details:
                    raise ValueError("Duplicate {} file for {}".format(
                        end, inputid
                    ))
                else:
                    inputfile_details[end_path_key] = filepath_str
                # provide some output to user
                print("   Adding file to input list:",filepath_str)
                print("    End:",end)
                print("    Run id:",inputfile_details['run_id'])
                print("    Base filename:",inputfile_details['input_basefilename'])
                print("    Unique input id:",inputid)
                if verbose:
                    print("   Current details for this inputfile:")
                    _pi_pp.pprint(inputfile_details)
            elif filename.endswith(tuple(input_extensions)):
                # no, but has correction extension
                # raise exception or warn as might be config problem
                message = "File {} has correct extension, but doesn't " \
                "match input file regex pattern - check input end1/end2 " \
                "suffixes.".format(filename)
                if not ignore_valid_extension_uncollected: raise ValueError(message)
                else: warn_user(message)

            else:
                if verbose: print("  Skipping file:",filename)
    # convert to pandas df
    input_file_df = pandas.DataFrame.from_dict(input_file_collection, orient="index")
    print("Obtained input file dataframe:")
    print(input_file_df.to_string())
    # do some sanity checks
    # 0) do we actually have anything in the dataframe?
    if len(input_file_df)==0:
        raise ValueError("No input files were found")
    # 1) we should always have no null end1 paths
    if input_file_df['end1_path'].isnull().any():
        raise ValueError("Some end1 files are missing")
    if paired_reads:
        # 2) if reading paired reads, ensure all end2 paths present unless
        #    we are ignoring any missing ones
        if 'end2_path' not in input_file_df:
            if ignore_missing_end2:
                warn_user("All end2 are missing, ignoring")
            else:
                raise ValueError("All end2 files are missing")
        elif input_file_df['end2_path'].isnull().any():
            if ignore_missing_end2:
                warn_user("Some end2 are missing, ignoring")
            else:
                raise ValueError("Some end2 files are missing")
    else:
        # 3) if not reading paired reads, we shouldn't have any end2 reads
        if 'end2_path' in input_file_df and input_file_df['end2_path'].notnull().any():
            raise ValueError("Not expecting pair reads, but some end2 files found")
    # 4) all inputids should be unique
    print(input_file_df['inputid'].unique())
    if len(input_file_df['inputid'].unique())<len(input_file_df['inputid']):
        # this probably can never occur due to above code, but is worth checking anyway
        raise ValueError("Not all inputid values are unique")
    # all good, return the df
    return input_file_df

# requires global options:
# sample_df: a pandas dataframe containing sample_name and input_regex columns
#  sample_name values must all be unique
# all columns in sample_df are added to sample_dict
def map_samples_to_inputid_list_default(input_file_df):
    # sanity check sample_df
    if len(sample_df['sample_name'].unique())<len(sample_df['sample_name']):
        raise ValueError("Not all sample_name values are unique")
    print("Mapping samples to input fastq files...")
    temp_sample_df_dict = sample_df.to_dict('index')
    inputid_dict = {}
    for input_file_index,input_file_details in input_file_df.iterrows():
        input_file_details_dict = dict(input_file_details.dropna())
            # has_end2 relies on existence of end2_path in dict,
            # so drop nas here to remove any invalid end2_path entries
        inputid = input_file_details_dict['inputid']
        for sample_index,sample_details in temp_sample_df_dict.items():
            sample_input_regex = sample_details['input_regex']
            if re.search(sample_input_regex,inputid):
                sample_name = sample_details['sample_name']
                if verbose: print("Matched sample {} with input {}.".format(
                    sample_name, inputid
                ))
                if not allow_multiple_samples_per_input and 'sample_name' in input_file_details_dict:
                    raise ValueError("Multiple samples mapped to input {}".format(
                    inputid
                    ))
                sample_name_list = input_file_details_dict.setdefault('sample_name',list())
                sample_name_list.append(sample_name)
                sample_inputid_list = sample_details.setdefault('inputid_list',list())
                sample_inputid_list.append(inputid)
        if 'sample_name' not in input_file_details_dict:
            raise ValueError("No samples mapped to input {}".format(
            inputid
            ))
        inputid_dict[inputid] = input_file_details_dict
    sample_dict = {}
    for sample_index,sample_details in temp_sample_df_dict.items():
        sample_name = sample_details['sample_name']
        print("Sample:",sample_name)
        if 'inputid_list' not in sample_details:
            raise ValueError("No inputs mapped to sample {}".format(
            sample_name))
        sample_details['inputid_list'] = sorted(sample_details['inputid_list'])
            # sort to give deterministic order
        print(" Input list:",', '.join(sample_details['inputid_list']))
        sample_dict[sample_name] = sample_details
    return inputid_dict,sample_dict

# print input and sample details
def print_input_sample_details_default():
    print("Full input dict follows:")
    _pi_pp.pprint(inputid_dict)
    print("Full sample dict follows:")
    _pi_pp.pprint(sample_dict)


# obtain original input file path from the following wildcards:
#  inputid, and end_label (end1 or end2)
def _get_original_input_path_default(wildcards):
    inputid = wildcards.inputid
    end_label = wildcards.end_label
    if inputid not in inputid_dict:
        raise ValueError("Could not lookup {} from inputid_dict".format(inputid))
    input_details = inputid_dict[inputid]
    if end_label==end1_label:
        return inputid_dict[inputid]['end1_path']
    elif end_label==end2_label:
        return inputid_dict[inputid]['end2_path']
    else:
        raise ValueError("Unknown end label {}".format(end_label))

# gets all inputids for a sample
def get_all_inputids_default(sample_name):
    return sample_dict[sample_name]['inputid_list']

# gets a list of {sample_name, inputid} values for all samples/inputid pairs
# can be used to as 'input' with above expand templates
def get_all_inputs_default():
    ret = []
    for input_details in inputid_dict.values():
        inputid = input_details['inputid']
        sample_names = input_details['sample_name']
        ret.extend(({'sample_name': sample_name, 'inputid' : inputid} for sample_name in sample_names))
    return ret

# returns true if inputid has valid end2 files
# only useful if ignore_missing_end2 is on
def has_end2_default(inputid):
    return 'end2_path' in inputid_dict[inputid]

# returns a function that takes wildcards as a parameter to expand all inputids for wildcards.sample_name
def expand_all_inputids_function_factory_default(template):
    if isinstance(template, PathTemplater):
        return lambda wildcards: template.expand(**wildcards, inputid = get_all_inputids(wildcards.sample_name))
    else:
        return lambda wildcards: expand(template, **wildcards, inputid = get_all_inputids(wildcards.sample_name))

try: read_sample_list
except: read_sample_list = read_sample_list_default

try: build_inputid_list
except: build_inputid_list = build_inputid_list_default

try: map_samples_to_inputid_list
except: map_samples_to_inputid_list = map_samples_to_inputid_list_default

try: print_input_sample_details
except: print_input_sample_details = print_input_sample_details_default

try: _get_original_input_path
except: _get_original_input_path = _get_original_input_path_default

try: get_all_inputids
except: get_all_inputids = get_all_inputids_default

try: get_all_inputs
except: get_all_inputs = get_all_inputs_default

try: has_end2
except: has_end2 = has_end2_default

try: expand_all_inputids_function_factory
except: expand_all_inputids_function_factory = expand_all_inputids_function_factory_default

sample_df = read_sample_list()

# sample_dict should contain sample_name: {inputid_list : [inputid1, inputid2...]} )
# inputid_dict should contain input_id: {end1_path, (end2_path), sample_name}
# these can be extended/modified if processing functions are overriden
inputid_dict,sample_dict = map_samples_to_inputid_list(build_inputid_list())
print_input_sample_details()

# stage inputs by generating input symlinks with correct naming scheme
_stage_input_templater = path_templater.create_fromparts(staged_input_directory, source_endx_template, staged_input_extension)

rule stage_input:
    input:
        _get_original_input_path
    output:
        _stage_input_templater.use()
    log:
        _stage_input_templater.logfile().use()
    shell:
        "ln -Tsrv {input} {output} "
        ">{log} 2>&1"

# sample reads from inputs using seqtk, piping into gzip to compress
_subsample_input_templater = path_templater.create_fromparts(subsampled_input_directory, source_endx_template, subsampled_input_extension)
rule subsample_input:
    input:
        rules.stage_input.output
    output:
        _subsample_input_templater.use()
    log:
        _subsample_input_templater.logfile().use()
    conda:
        "envs/seqtk.yaml"
    shell:
        "seqtk sample "
        "-s {config[subsample__rng_seed]} "
        "{config[subsample__additional_parameters]} "
        "{input} "
        "{config[subsample__frac_number]} "
        "| gzip {config[subsample__gzip_parameters]} > {output} "
        "2>{log}"

# combine input files using cat (possible even if files are gzipped)
rule sample_combine_input:
    input:
        expand_all_inputids_function_factory(input_source_template)
    output:
        combined_input_source_template.use()
    log:
        combined_input_source_template.logfile().use()
    shell:
        "cat {input}  > {output} "
        "2>{log}"
