# `prepare_input`

Contains generic Snakemake rules and associated functions for preparing
input sequencing data for subsequent processing.

Note: This is the README for `prepare_input`, which is used within other
workflows. If this file is located within another workflow, there may be
another README file for that workflow itself.

This workflow identifies all input files (matching them as read pairs), gives
each input a standard and unique inputid and associates each input with a
sample based on a sample list. Inputs are then 'staged' to easily be selected
by a downstream rule. An option to subsample inputs is provided, in which case
staged or substampled inputs can be automatically selected. The workflow
provides several helper functions and `PathTemplater` objects for easily
generating input and output paths in downstream steps.

Requires: [`pathtemplater`](https://pypi.org/project/pathtemplater/). This can
be installed with `conda install pathtemplater -c twlee79`.

# Usage

Include this file in a downstream workflow using the `include:` tag. It requires
a number of config parameters; default parameters are provided in the
`prepare_input_default_config.yaml` file. Specify this as the first
`configfile:` loaded to allowed these to be overridden by workflow defaults
or user parameters in downstream `configfile`s.

## Inputs

Input files should be placed in the `input_root_directory`, within
subdirectories named by a sequencing `run_id`. Files can be in any directory
structure within those subdirectories, with all lower-level subdirectory names
ignored. The workflow will collect all files within that directory structure
that contain one of the `input_extensions` and match either on
`input_end1_suffix` or `input_end2_suffix`, which are filename suffixes
expected before the extension. By default, if there are files matching
an `input_extensions` but not matching `input_end1_suffix` or
`input_end2_suffix`, an exception is raised as this is likely a configuration
error. Collected files are then sorted into matching paired reads based on
`input_end1_suffix` and `input_end2_suffix`. The rest of the filename,
excluding the extension and `input_endx_suffix` is
used as the `input_basefilename`. An `inputid` for the input file (pair) will be
generated as `run_id__input_basefilename` (`run_id` is name of the top-level
subdirectory containing the file within the `input_root_directory`). The
`inputid` must be unique for all input file (pairs). It is used as identifier
for each input file (pair) that has a consistent naming scheme and that allows
all downstream output files generated from the input file (pair) to be stored
in a single directory.

By default, paired-reads are expected (`paired_reads: yes` option)
and matching end1 and end2 files are required for all `inputid`s
(`ignore_missing_end2: no`). To allow missing end2 files, the
`ignore_missing_end2: yes` option can be used. The `has_end2(inputid)`
helper function can then be used to determine if individual `inputid` has a
end2 file associated with it. To use single-end reads, use the
`paired_reads: no` option and an empty (or universal) `input_end1_suffix` to
match all input files as end1. To catch configuration errors, no files that
match only the `input_end2_suffix` can be present with `paired_reads: no`,
Similarly, all files with matching `input_extensions` must contain a valid
`input_end1_suffix` or `input_end2_suffix`, as noted above, unless
`ignore_valid_extension_uncollected: yes` is used.

## Samples

Each input is mapped to sample based on the table provided in
`sample_list_path`. The relevant columns in this file are `sample_name` and
`input_regex`. Sample names (`sample_name`) are mapped to `inputid`s using the
sample's corresponding `input_regex` value as a regular expression pattern;
`inputids` that are match the pattern with the `re.search()` function are
assigned to the sample. All `sample_name` values must be unique. All `inputid`
values must be assigned to a sample. Multiple `inputid`s may be mapped to each
sample, for example, data from different sequencing lanes in different input
files (thus having different `inputid`s).  If
`allow_multiple_samples_per_input` is off (default), each `inputid` can only
match a single sample. If `allow_multiple_samples_per_input` is activated,
multiple samples can map to a single `inputid`; users should handle this
downstream to ensure the data from each `inputid` is divided for the samples,
e.g. by demultiplexing using barcodes, otherwise the data could end up being
reused for multiple samples.

## Sample and input collections

Two `dict`s are produced after sample mapping:

* `inputid_dict` which maps `inputid` keys to a `dict` containing `end1_path`,
`end2_path` and `sample_name`. Note that `sample_name` is a list of
`sample_name`s mapped to each `inputid`, even if only a single `sample_name`
is mapped to each `inputid` (e.g. `allow_multiple_samples_per_input` is off).
`get_all_inputs()` can be used to generate a list of all
`{sample_name, inputid}` mappings.

* `sample_dict` wich maps `sample_name` keys to a `dict` containing
`inputid_list` which is a list of `inputid`s for the sample.

These can be used downstream to expand lists of output files, e.g. expanding
all samples using `expand(sample_name = sample_dict.keys())`, or expanding
all inputs using `expand(input=get_all_inputs())` and
`sample_inputid_expand_template`.

## Subsampling inputs
To quickly test a workflow with a subsample of input reads, the
`subsample_input: yes` option can be used. The reads will then be subsampled
by the `rule subsample_input:` using `seqtk` into `subsampled_input_directory`
and wth. Additional parameters for this rule are present in the config file.

## Sourcing inputs
A user can select the the correct inputs (staged or subsampled) using
`input_source_directory` and `input_source_extension` together with
`sample_inputid_endx_template`, or the provided `input_source_template`
`PathTemplater` object (see below for more details). In most usage, this will
be the `input:` for the first downstream rule.

## Directory structure and `PathTemplater` objects
This base workflow defines a generic directory structure for use in downstream
steps, and uses `PathTemplater` objects from `pathtemplater` to aid
deriving input, output and log file paths within this directory structure.
The directory structure is:

* Input directory: top-level directory containing all original unprocessed inputs.

* Intermediate directory: top-level directory containing all intermediate
processing stages, files for each stage should be within a subdirectory.

* Output directory: top-level directory containing final outputs, suggest
placing these within a subdirectory even if there is only a single set of
final outputs.

* Log directory: all logs go within here with subdirectories of the same
name as those in intermediate or output directories. A useful definition of a
log file is a small text file that provide information regarding the processing
at a particular step, and that is not subsequently processed by a downstream
step.

The following pregenerated `PathTemplater` objects are defined:

* `pathtemplater`: Top-level `PathTemplater` object. This contains `inputdir()`,
`intermediatedir()`, `outputdir()`, `logdir()` functions for setting top
directory, `logfile()` function for creating a log file in the `logdir` with
`.log` suffix, and `end1()`, `end2()`, `end1_2()` and `expand_ends()` presets
for easily formatting and expanding `{end_label}` wildcards. This must be
initialized with `create()` before use.

* `input_source_template`: `PathTemplater` object derived from `pathtemplater`,
provides a template to select staged or subsampled inputs with `sample_name`,
`inputid` and `end_label` wildcards.

* `combined_input_source_template`: `PathTemplater` object derived from
`pathtemplater`, provides a template to select combined inputs with
`sample_name` and `end_label` wildcards.

## Combining inputs per sample
A rule for combining all inputs for a sample is provided
(`rule sample_combine_input:`). This rule will combine either staged or
subsampled inputs. To source the combined inputs, use the
`combined_input_source_endx_template`, which sources combined inputs based
on `sample_name` and `end_label` wildcards. If subsampled inputs are combined,
the combined inputs may be biased as a greater proportion of reads from input
files with fewer reads are selected; to avoid this fractional subsampling
should be used (e.g.`subsample__frac_number: 0.05`).

## Combining inputs downstream
To combine all inputs for a sample downstream,
`expand_all_inputids_function_factory(template)` can be used as an input. This
returns a lambda function that takes `wildcards` as a parameter and expands
all `{inputid}` wildcards for `wildcards.sample_name`.

## Additional helpers
This workflow defines the `verbose` boolean value that is set in the config file
and can be used to provide more verbose output in a workflow. The
`warn_user(message)` function provides a mechanism to output a warning message
to users. The `print_all_rules()` function will print the input, output, log
and param details of all rules after formatting. This is very useful for
debugging a workflow, as the exact paths in each step may not be immediately
apparent when `PathTemplater` objects are used. A useful pattern is to add
`if verbose: print_all_rules()` to end of main Snakefile to print all rules
if `verbose` is enabled.

---

# Implementation details

## Generating the input list

When loaded, the scripts in this file will by default walk the
`input_root_directory` to find all files that contain one of the
`input_extensions` and sort this into matching paired-read end1 and end2
files based on `input_end1_suffix` and `input_end1_suffix`.
An `inputid` for the input file (pair) will be
generated as noted above. At the end of the directory walk, a list of all
`inputid`s is obtained matched to the paths of the end1 and end2 input files.

## Matching inputs to sample names

Once the `inputid` list is generated, these are matched to samples. Sample
names are provided in file specified by `sample_list_path`. This is read as
a pandas dataframe, and should contain the `sample_name` and `input_regex`
columns. Samples are mapped to `inputid`s as noted above.

After mapping, two `dict`s will be produced:

* `inputid_dict` which maps `inputid` keys to a `dict` containing `end1_path`,
`end2_path` and `sample_name`.

* `sample_dict` wich maps `sample_name` keys to a `dict` containing
`inputid_list` which is a list of `inputid`s for the sample.


## Staging inputs
To ensure the inputs are in the same flat directory structure as outputs,
`rule stage_input:` will generate symlinks within the directory specified
with the `staged_input_directory:` option (by default `in_staged_data`) named
as `{sample_name}___{inputid}_{end_label}` and with extension specified by
the `staged_input_extension:` option (default: `.fastq.gz`).

## Overriding functions
All functions used in this workflow are defined with a suffix `_default`. If
any existing function is predefined (before the workflow is included) without
the `_default` suffix, it will be used instead. This provides a means of
overriding any functions in this workflow without altering its code directly.

---

### Additional details

* Author:       Tet Woo Lee
* Copyright:    © 2019 Tet Woo Lee
* Licence:      GPLv3
* Dependencies: [`pathtemplater`](https://pypi.org/project/pathtemplater/)
(tested with v1.0.0.dev7).

### Change log

v1.0.0.dev10 2020-03-29
 * Add toptemplatedir() giving `{topdirectory}` template for generic 
   templaters

v1.0.0.dev9 2020-03-10
 * Fixed error in print_all_rules_default() with using Snakemake 5.10

v1.0.0.dev8 2019-06-22
 * Fixed bug in has_end2

v1.0.0.dev7 2019-02-18
 * Added get_all_inputs(), now needed as sample_name is store as list in
 inputid_dict

v1.0.0.dev6 2019-02-13
 * Added has_end2()
 * Improved sanity checks for unexpected conditions when building input file
   list

v1.0.0.dev5 2019-02-13
 * Fix nasty bug causing only L001 to be used

v1.0.0.dev4 2019-02-12
 * Add inputid wildcard_constraints

v1.0.0.dev3 2019-02-12
 * Added allow_multiple_samples_per_input

v1.0.0.dev2 2019-02-08
 * Updated for PathTemplater v1.0.0.dev7

v1.0.0.dev1 2019-02-07
 * Complete version, tested and working with PathTemplater v1.0.0.dev5

v0.1
 * First working version
