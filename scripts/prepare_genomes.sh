# download igenomes, untar and build hisat2 indexes, copy gtf files
# run from analysis root directory

# set download_dir,  number of cores to use, and various output directories
# EDIT AS APPROPRIATE
download_dir=/data/Downloads
in_genomes_dir=input/genomes
hg38_index_dir=${in_genomes_dir}/igenome_hg38_hisat2
mm10_index_dir=${in_genomes_dir}/igenome_mm10_hisat2
gtf_dir=${in_genomes_dir}/igenome_gtf
cores=4
igenomes_untar_dir=${in_genomes_dir}/igenomes
temp_env_name=hnscc_clxpdx_hisat2
info_file=${in_genomes_dir}/genomes_from.txt

# igenome info: https://support.illumina.com/sequencing/sequencing_software/igenome.html
# download hg38: ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Homo_sapiens/UCSC/hg38/Homo_sapiens_UCSC_hg38.tar.gz
wget -c ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Homo_sapiens/UCSC/hg38/Homo_sapiens_UCSC_hg38.tar.gz -P ${download_dir}
# download mm10: ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Mus_musculus/UCSC/mm10/Mus_musculus_UCSC_mm10.tar.gz
wget -c ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Mus_musculus/UCSC/mm10/Mus_musculus_UCSC_mm10.tar.gz -P ${download_dir}

# untar
mkdir -p ${igenomes_untar_dir}
tar -xvzf ${download_dir}/Homo_sapiens_UCSC_hg38.tar.gz -C ${igenomes_untar_dir}
mv ${igenomes_untar_dir}/README.txt ${igenomes_untar_dir}/Homo_sapiens_UCSC_hg38_README.txt
tar -xvzf ${download_dir}/Mus_musculus_UCSC_mm10.tar.gz -C ${igenomes_untar_dir}
mv ${igenomes_untar_dir}/README.txt ${igenomes_untar_dir}/Mus_musculus_UCSC_mm10_README.txt

# build hisat2 indexes
conda env create --name ${temp_env_name} --file envs/hisat.yaml
source activate ${temp_env_name}
mkdir -p ${hg38_index_dir}
hisat2-build ${igenomes_untar_dir}/Homo_sapiens/UCSC/hg38/Sequence/WholeGenomeFasta/genome.fa ${hg38_index_dir}/genome -p ${cores}
mkdir -p ${mm10_index_dir}
hisat2-build ${igenomes_untar_dir}/Mus_musculus/UCSC/mm10/Sequence/WholeGenomeFasta/genome.fa ${mm10_index_dir}/genome -p ${cores}
mkdir -p ${gtf_dir}
cp ${igenomes_untar_dir}/Homo_sapiens/UCSC/hg38/Annotation/Genes/genes.gtf ${gtf_dir}/genes_hg38_RefSeq.gtf
cp ${igenomes_untar_dir}/Homo_sapiens/UCSC/hg38/Annotation/Genes.gencode/genes.gtf ${gtf_dir}/genes_hg38_GENCODE.gtf
cp ${igenomes_untar_dir}/Homo_sapiens/UCSC/hg38/Annotation/Archives/archive-current/README.txt ${gtf_dir}/genes_hg38_README.txt
cp ${igenomes_untar_dir}/Mus_musculus/UCSC/mm10/Annotation/Genes/genes.gtf ${gtf_dir}/genes_mm10_RefSeq.gtf
cp ${igenomes_untar_dir}/Mus_musculus/UCSC/mm10/Annotation/Archives/archive-current/README.txt ${gtf_dir}/genes_mm10_README.txt
source deactivate
conda remove --name ${temp_env_name} --all -y

# write info_file
echo "${hg38_index_dir} generated from (md5):" >${info_file}
md5sum ${download_dir}/Homo_sapiens_UCSC_hg38.tar.gz >>${info_file}
echo "${mm10_index_dir} generated from (md5):" >>${info_file}
md5sum ${download_dir}/Mus_musculus_UCSC_mm10.tar.gz >>${info_file}
echo "gtk files from same files" >>${info_file}

# ${igenomes_untar_dir} can be removed if desired
# rm -r ${igenomes_untar_dir}
