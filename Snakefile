configfile: "prepare_input_default_config.yaml" # defaults for prepare_input.smk
configfile: "default_config.yaml"  # default configuration parameters
configfile: "override_config.yaml" # overrides any default configuration parameters

include: "prepare_input.smk" # use prepare_input generic workflow to read sample_list and get list of input files

# pretty printer
pi_pp = pprint.PrettyPrinter(indent=4)

# genome list
genomeA_name = config['genomeA_name']
genomeB_name = config['genomeB_name']
genome_list = [genomeA_name, genomeB_name]

# add suffixes and presets to templater objects
for templater in (path_templater,input_source_template):
    templater.add_alt_suffixes(config['path_templater_additional_extensions'])
    templater.add_preset_formats({'fastqc_file': {'logdir': ([],{}), 'new_affix' : (["_fastqc"],{}), 'new_suffix':([".html"],{}) },
                                  'all_inputs': {'apply_format': ([],{'inputid':"all"}) }
                                 })

# simple templaters for templating without sample_names etc.
basic_templater = path_templater.create("{directory}/{filename}")
    # simple directory/filename templater
    # defaults to intermediate dir

basic_fastq_gz_templater = basic_templater.fastq_gzfile()
basic_fastqc_templater = basic_templater.fastqc_file()
basic_bam_templater = basic_templater.bamfile()

# template strings with genome appended
sample_inputid_genome_template = sample_inputid_template+"_{genome}"
sample_inputid_genome_expand_template = sample_inputid_expand_template+"_{genome}"
sample_inputid_endx_genome_template = sample_inputid_genome_template + "_" + end_label_wildcard

# parse inputid into illumina naming elements
# used for adding details to bamfiles
# Illumina naming convention {SampleName}_{SXX SampleNumber}_{LXXX LaneNumber}_{RX Read}_001
parse_illumina_name = lambda sample_name__inputid: re.match("(?P<run>.*)__(?P<illumina_sample_name>.+)_(?P<illumina_sample_number>S\d+)_(?P<illumina_lane_number>L\d+)",sample_name__inputid).groupdict()

# genome filename templates
genome_base_templater = path_templater.inputdir().create_fromparts(config['genome_base_directory'],"","")
genome_hisat2_index_template = genome_base_templater.new_template(config['genome_hisat2_index_wildcard_template'])
genome_gtf_template = genome_base_templater.new_template(config['genome_gtf_wildcard_template'])

# templates for all steps
fastq_stats_inputs_template = input_source_template.fastq_statsfile()
fastqc_inputs_template = input_source_template.fastqc_file()

trim_worker_out_template = input_source_template.new_directory(config['trim_output_directory']).new_suffix(".fq.gz")
trim_out_template = trim_worker_out_template.fastq_gzfile()
trim_stats_template = trim_worker_out_template.fastq_statsfile()

umi_extractlabels_out_template = trim_out_template.new_directory(config['umi_extractlabels_output_directory']).new_affix("_extractlabels")
umi_extractlabels_fastqc_template = umi_extractlabels_out_template.apply_affix().fastqc_file() # apply _extractlabels so that _fastqc can be added

trim_fastqc_template = trim_out_template.logdir().fastqc_file()
umi_extractlabels_stats_template = umi_extractlabels_out_template.fastq_statsfile()

hisat2_allreads_out_template = path_templater.create_fromparts(config['hisat2_allreads_output_directory'],
    sample_inputid_genome_template,filename_affix = "_hisat2", suffix = ".sorted.bam")

disambiguate_worker_out_template = path_templater.create_fromparts(config['disambiguate_output_directory'], sample_inputid_template,".bam")
disambiguate_out_template = hisat2_allreads_out_template.new_directory(config['disambiguate_output_directory']).new_suffix(".bam").new_affix("_disambiguated")
disambiguate_ambig_out_template = disambiguate_out_template.new_affix("_ambiguous")
disambiguate_worker_summary_template = disambiguate_worker_out_template.new_affix("_summary").new_suffix(".txt")

create_disambiguated_fq_out_template =  disambiguate_out_template.new_directory(config['create_disambiguated_fq_output_directory']).new_template(sample_inputid_endx_genome_template).fastq_gzfile()
disambiguated_stats_inputs_template = create_disambiguated_fq_out_template.fastq_statsfile()

hisat2_disamb_align_out_template = hisat2_allreads_out_template.new_directory(config['hisat2_disamb_align_output_directory'])
combine_bam_out_template = hisat2_disamb_align_out_template.all_inputs().new_directory(config['combine_bam_output_directory']).new_affix("_combined")

umi_dedup_out_templater = combine_bam_out_template.new_directory(config['umi_dedup_output_directory']).new_affix("_dedup")
umi_dedup_stats_out_templater = umi_dedup_out_templater.logdir()

feature_count_out_templater = combine_bam_out_template.new_directory(config['feature_count_output_directory']).outputdir().new_affix("_counts").tsvfile()

analysis_complete_templater = path_templater.create_fromparts(config['summarise_logs_output_directory'], config['summarise_logs_base_filename'],"").outputdir().completefile()
summary_out_templater = path_templater.create_fromparts(config['summarise_logs_output_directory'], config['summarise_logs_base_filename'],"").outputdir().new_affix("_summary").new_suffix(".tsv")


# runs full workflow and generates summary
rule all:
    input:
        analysis_complete_templater.use(),
        summary_out_templater.use() # comment out to remove summary

# executes full workflow
# output can be linked to a run summarisation step
rule run_all:
    input:
        feature_count_out_templater.expand(sample_name = sample_dict.keys(), genome=genome_list) # final output
        #hisat2_disamb_align_out_template.new_template(sample_inputid_genome_expand_template).expand(input=get_all_inputs(), genome=genome_list) # disamb hisat2
        #expand_all_input_sources() # staged inputs
        #expand_all_combined_input_sources() # combined inputs
    output:
        touch(analysis_complete_templater.use())

#######
# Rules
#######

# generic rule to generate stats on reads and bases in a fastqc.gz file
rule generic_fastq_stats:
    input:
        basic_fastq_gz_templater.use()
    output:
        basic_fastq_gz_templater.fastq_statsfile().use()
    shell:
        "scripts/summarise_fq.sh {input} >{output}"

# generic rule to run fastqc on a fastqc.gz file
rule generic_fastqc:
    input:
        basic_fastq_gz_templater.use()
    output:
        basic_fastqc_templater.use()
    log:
        basic_fastqc_templater.logfile().use()
    params:
        output_dir = basic_fastqc_templater.get_directory()
    conda:
        "envs/fastqc.yaml"
    shell:
        "fastqc -o {params.output_dir} {input} "
        ">{log} 2>&1"

# chain to this rule to get stats on inputs
rule fastq_stats_inputs:
    input:
        fastq_stats_inputs_template.expand_ends()
    output:
        touch(fastq_stats_inputs_template.end1_2().completefile().use())

# chain to perform fastqc on input reads
rule fastqc_inputs:
    input:
        fastqc_inputs_template.expand_ends()
    output:
        touch(fastqc_inputs_template.end1_2().completefile().use())


# perform trim for quality and illumina adaptors
    # trim_galore has this annoying fixed file naming scheme
rule trim_worker:
    input:
        input_source_template.expand_ends(),
        rules.fastq_stats_inputs.output,
        rules.fastqc_inputs.output,  # chain to run
    output:
        R1 = trim_worker_out_template.end1().new_affix("_val_1").use(),
        R2 = trim_worker_out_template.end2().new_affix("_val_2").use()
    log:
        trim_worker_out_template.end1_2().logfile().use()
    params:
        output_dir = trim_worker_out_template.get_directory()
    conda:
        "envs/trim_galore.yaml"
    shell:
        "trim_galore "
        "--illumina --paired "
        "-q {config[trim_quality_threshold]} "
        "--length {config[trim_length_threshold]} "
        "-o {params.output_dir} "
        "{config[trim_additional_parameters]} "
        "{input[0]} {input[1]}"
        " >{log} 2>&1"

# will run trim_worker, but convert non-standard file naming scheme of
# trim_galore output to standard
rule trim:
    input:
        **rules.trim_worker.output
    output:
        R1 = trim_out_template.end1().use(),
        R2 = trim_out_template.end2().use(),
    shell:
        """
        mv {input.R1} {output.R1}
        mv {input.R2} {output.R2}
        """

# chain to this rule to get stats of trimmed reads
rule trim_stats:
    input:
        stats_R1 = trim_stats_template.end1().use(),
        stats_R2 = trim_stats_template.end2().use(),
    output:
        touch(trim_stats_template.end1_2().completefile().use())

# umi_tools to extract molecular labels and place onto read name for later dedup
# there are 96 8-bp molecular labels in the NEXTflex Rapid Directional qRNA-seq Kit,
# all followed by a T
# umi_tools does not allow whitelisting of UMIs, but they can be matched by regex
rule umi_extractlabels:
    input:
        rules.trim_stats.output,
        **rules.trim.output,
    output:
        R1 = umi_extractlabels_out_template.end1().use(),
        R2 = umi_extractlabels_out_template.end2().use(),
    log:
        umi_extractlabels_out_template.end1_2().logfile().use()
    conda:
        "envs/umi_tools.yaml"
    shell:
        "umi_tools extract --extract-method=regex "
        "--bc-pattern={config[umi_extractlabels_pattern]} "
        "--bc-pattern2={config[umi_extractlabels_pattern]} "
        "-I {input.R1} --read2-in={input.R2} "
        "--stdout={output.R1} --read2-out={output.R2} "
        "--log={log}"
        "{config[umi_extractlabels_additional_parameters]} "


# chain to this rule to get stats of umi_extractlabels output
rule umi_extractlabels_stats:
    input:
        stats_R1 = umi_extractlabels_stats_template.end1().use(),
        stats_R2 = umi_extractlabels_stats_template.end2().use(),
    output:
        touch(umi_extractlabels_stats_template.end1_2().completefile().use())

# select either trimmed or trimmed_extracted output for subsequent steps
# depending on if use_umi_labels
def select_trimmed_output():
    if config['use_umi_labels']:
        ret = dict(
            R1 = umi_extractlabels_out_template.end1().use(),
            R2 = umi_extractlabels_out_template.end2().use(),
            stats_R1 = umi_extractlabels_stats_template.end1().use(),
            stats_R2 = umi_extractlabels_stats_template.end2().use(),
            fastqc_R1 = umi_extractlabels_fastqc_template.end1().use(),
            fastqc_R2 = umi_extractlabels_fastqc_template.end2().use(),
        )
    else:
        ret = dict(
            R1 = trim_out_template.end1().use(),
            R2 = trim_out_template.end2().use(),
            stats_R1 = trim_stats_template.end1().use(),
            stats_R2 = trim_stats_template.end2().use(),
            fastqc_R1 = trim_fastqc_template.end1().use(),
            fastqc_R2 = trim_fastqc_template.end2().use(),
        )
    return ret


# perform hisat2 alignment with a genome
# pipe output to convert to bam, then sort
rule hisat2_allreads:
    input:
        **select_trimmed_output()
    output:
        sorted_bam = hisat2_allreads_out_template.use()
    log:
        summary = hisat2_allreads_out_template.summaryfile().use(),
        stderr = hisat2_allreads_out_template.logfile().use(),
    params:
        index = genome_hisat2_index_template.use()
    conda:
        "envs/hisat.yaml"
    threads: 12
    shell:
        "hisat2 -p {threads} --summary-file {log.summary} "
        "{params.index} "
        "-1 {input.R1} -2 {input.R2} "
        "--new-summary "
        "{config[hisat2_allreads_additional_parameters]} "
        "| samtools view -b -@ {threads} - "
        "| samtools sort -n -@ {threads} - > {output.sorted_bam} "
        " 2>{log.stderr}"

# disambiguate sorted alignments
# worker that runs command
rule disambiguate_worker:
    input:
        genomeA = hisat2_allreads_out_template.pformat(genome=genomeA_name),
        genomeB = hisat2_allreads_out_template.pformat(genome=genomeB_name),
    output:
        genomeA = disambiguate_worker_out_template.apply_affix().new_affix(".disambiguatedSpeciesA").use(),
        genomeB = disambiguate_worker_out_template.apply_affix().new_affix(".disambiguatedSpeciesB").use(),
        genomeA_ambig = disambiguate_worker_out_template.apply_affix().new_affix(".ambiguousSpeciesA").use(),
        genomeB_ambig = disambiguate_worker_out_template.apply_affix().new_affix(".ambiguousSpeciesB").use(),
        summary = disambiguate_worker_summary_template.use()
    log:
        stdout = disambiguate_worker_out_template.logfile().use()
    params:
        prefix = disambiguate_worker_out_template.no_suffix().get_filename(),
        output_dir = disambiguate_worker_out_template.get_directory()
    conda:
        "envs/disambiguate.yaml"
    shell:
        "ngs_disambiguate "
        "-s {params.prefix} "
        "-o {params.output_dir} "
        "-a hisat2 {input.genomeA} {input.genomeB} "
        "{config[disambiguate_additional_parameters]} "
        " >{log.stdout} 2>&1"

# calls worker, produces output with standard filenames
rule disambiguate:
    input:
        **rules.disambiguate_worker.output
    output:
        genomeA = disambiguate_out_template.pformat(genome=genomeA_name),
        genomeB = disambiguate_out_template.pformat(genome=genomeB_name),
        genomeA_ambig = disambiguate_ambig_out_template.pformat(genome=genomeA_name),
        genomeB_ambig = disambiguate_ambig_out_template.pformat(genome=genomeB_name),
    shell:
        """
        mv {input.genomeA} {output.genomeA}
        mv {input.genomeB} {output.genomeB}
        mv {input.genomeA_ambig} {output.genomeA_ambig}
        mv {input.genomeB_ambig} {output.genomeB_ambig}
        """


# convert disambiguated bam files to fq format for each species
# these fq files can then be used in subsequent processing for each species
rule create_disambiguated_fq:
    input:
        disambiguate_out_template.use()
    output:
        R1 = create_disambiguated_fq_out_template.end1().use(),
        R2 = create_disambiguated_fq_out_template.end2().use(),
    log:
        create_disambiguated_fq_out_template.logfile().end1_2().use(),
    conda:
        "envs/picard.yaml"
    shell:
        "picard SamToFastq "
        "I={input} "
        "F={output.R1} "
        "F2={output.R2} "
        "validation_stringency={config[create_disambiguated_fq_validation_stringency]} "
        "{config[create_disambiguated_fq_additional_parameters]} "
        ">{log} 2>&1 "

# chain to this rule to get stats of create_disambiguated_fq files
rule disambiguated_stats:
    input:
        R1 = disambiguated_stats_inputs_template.end1().use(),
        R2 = disambiguated_stats_inputs_template.end2().use()
    output:
        touch(disambiguated_stats_inputs_template.end1_2().completefile().use())


# realign disambiguated fastq files to each genome
rule hisat2_disamb_align:
    input:
        rules.disambiguated_stats.output,
        **rules.create_disambiguated_fq.output
    output:
        sorted_bam = hisat2_disamb_align_out_template.use()
    log:
        summary = hisat2_disamb_align_out_template.summaryfile().use(),
        stderr = hisat2_disamb_align_out_template.logfile().use()
    params:
        index = genome_hisat2_index_template.use(),
        id = lambda wildcards: "{sample_name}.{illumina_lane_number}".format(**wildcards,**parse_illumina_name(wildcards.inputid)) if (config['hisat2_disamb_align_add_lane_to_id']) else "{sample_name}".format(**wildcards),
        LB = "{sample_name}.L1",  # library, one per sample
        SM = "{sample_name}",
        PL = "ILLUMINA"
    conda:
        "envs/hisat.yaml"
    threads: 12
    shell:
        "hisat2 -p {threads} --summary-file {log.summary} "
        "{params.index} "
        "-1 {input.R1} -2 {input.R2} "
        "--new-summary --dta --rna-strandness=FR "
        "--rg-id={params.id} --rg LB={params.LB} "
        "--rg SM={params.SM} --rg PL={params.PL} "
        "{config[hisat2_disamb_align_additional_parameters]} "
        "| samtools view -bh -@ {threads} - "
        "| samtools sort -@ {threads} - > {output.sorted_bam} "
        " 2>{log.stderr}"

# index a sorted bam file
rule bam_index:
    input:
        basic_bam_templater.use()
    output:
        basic_bam_templater.baifile().use()
    conda:
        "envs/hisat.yaml"
    threads: 12
    shell:
        "samtools index -@ {threads} -b {input} {output}"



# combine all lanes into one file
rule combine_bam:
    input:
        expand_all_inputids_function_factory(hisat2_disamb_align_out_template)
    output:
        combined_bam = combine_bam_out_template.use()
    conda:
        "envs/hisat.yaml"
    threads: 12
    shell:
        "samtools merge -@ {threads} {output} {input}"

# umi_tools to dedup mapped reads
rule umi_dedup:
    input:
        **rules.combine_bam.output,
        index = combine_bam_out_template.baifile().use()
    output:
        dedup_bam = umi_dedup_out_templater.use(),
        stats_per_umi_per_position = umi_dedup_stats_out_templater.new_affix("_per_umi_per_position").tsvfile().use(),
        stats_per_umi = umi_dedup_stats_out_templater.new_affix("_per_umi").tsvfile().use(),
        stats_edit_distance = umi_dedup_stats_out_templater.new_affix("_edit_distance").tsvfile().use(),
    log:
        log = umi_dedup_out_templater.logfile().use()
    conda:
        "envs/umi_tools.yaml"
    params:
        output_stats_prefix = umi_dedup_stats_out_templater.remove_affix().no_suffix().use()
    shell:
        "umi_tools dedup "
        # use "unique" method as our UMIs are defined tags (not random)
        "--method unique --paired "
        "-I {input.combined_bam} -S {output.dedup_bam} "
        "--output-stats={params.output_stats_prefix} "
        "--log={log.log} "
        "{config[umi_dedup_additional_parameters]} "

def select_feature_count_input():
    if config['use_umi_labels']:
        bam = rules.umi_dedup.output.dedup_bam
    else:
        bam = rules.combine_bam.output.combined_bam
    return bam

feature_count_summary_templater = feature_count_out_templater.new_suffix('+.summary')
# do feature count
rule feature_count:
    input:
        bam = select_feature_count_input(),
        gtf = genome_gtf_template.use()
    output:
        count = feature_count_out_templater.use(),
        summary = feature_count_summary_templater.use()
    log:
        feature_count_out_templater.logfile().use()
    conda:
        "envs/subread.yaml"
    shell:
        "featureCounts -T {threads} -p "
        "-S {config[feature_count_strandness]} "
        "-t {config[feature_count_feature_type]} "
        "-g {config[feature_count_attribute_type]} "
        "-a {input.gtf} -o {output.count} {input.bam} "
        "{config[feature_count_additional_parameters]} "
        "  >{log} 2>&1"

################
# Summarisation
################

# return single-quoted string
str_singlequote = lambda string: "'{}'".format(string)

# use a templater to generate a pathname contaning glob-style wildcards
# i.e. named wildcards replaced with *
# e.g. "logs/1_trimmed/{sample_name}___{inputid}_{end_label}.fastq.stats"
# to "logs/1_trimmed/*___*_*.fastq.stats"
def make_glob(path_templater):
    the_glob = path_templater.format(sample_name = '*', inputid = '*', genome = '*', end_label = '*')
    return the_glob

# use a templater to generate regex for use by abrije
# the returned regex captures captures:
#   * the directory as the 'step' tag
#   * sample_name and inputid as labels
#   * genome and end as tags
#   * uses the provided parse type to specify the type
# e.g. ("logs/1_trimmed/{sample_name}___{inputid}_{end_label}.fastq.stats", "type_tsv_firstrow")
# to "logs/(?P<tag_output>[^/]+)/(?P<label_1sample>[^/.]+)___(?P<label_2inputid>[^/.]+)_(?P<tag_end>R[1-2]).(?P<type_tsv_firstrow>fastq.stats)"
# Note: original characters aren't regex escaped here; so take care
# to ensure these are tolerated by resulting regex
def make_abrije_regex(path_templater,parse_type):
    cur_suffix = path_templater.suffix
    new_suffix = re.sub("([.])(.*)","\\1(?P<{}>\\2)".format(parse_type),cur_suffix)
        # add type named tag to suffix
        # need to keep dot at start as PathTemplater requires this for a suffix
    the_regex = path_templater.new_directory( # capture directory as step tag
                "(?P<tag_step>[^/]+)").new_suffix(new_suffix).format(
                    sample_name = '(?P<label_1sample>[^/.]+)',
                    inputid = '(?P<label_2inputid>[^/.]+)',
                    genome = '(?P<tag_genome>[^/_.]+)',
                    end_label = '(?P<tag_end>R[1-2])')
        # tag/label wildcards, limiting regexes as needed
    return the_regex

# turn a list of path_templater objects into a list of glob strings, single quoted
def make_globs(path_templater_list):
    the_globs = [make_glob(templater) for templater, parse_type in path_templater_list]
    return list(map(str_singlequote,the_globs))

# turn a list of path_templater objects into a list of regex strings, single quoted
def make_abrije_regexes(path_templater_list):
    the_regexes = [make_abrije_regex(templater, parse_type) for templater, parse_type in path_templater_list]
    return list(map(str_singlequote,the_regexes))

# put together templater list
templater_list = [(fastq_stats_inputs_template, "type_tsv_firstrow"),
                  (trim_stats_template, "type_tsv_firstrow")]
if config['use_umi_labels']:
    templater_list.append((umi_extractlabels_stats_template, "type_tsv_firstrow"))
templater_list.extend([
        (hisat2_allreads_out_template.summaryfile(),"type_hisat2_summary"),
        (disambiguate_worker_summary_template,"type_tsv_firstrow"),
        (disambiguated_stats_inputs_template,"type_tsv_firstrow"),
        (hisat2_disamb_align_out_template.summaryfile(),"type_hisat2_summary")
        ])
if config['use_umi_labels']:
    templater_list.append((umi_dedup_out_templater.logfile(), "type_umi_tools_dedup"))
templater_list.extend([
        (feature_count_summary_templater, "type_tsv_rows"),
        ])

# turn list into globs/regexes
the_globs = make_globs(templater_list)
the_regexes = make_abrije_regexes(templater_list)
if verbose:
    print("For summarise, full list of abrije globs:")
    pi_pp.pprint(the_globs)
    print("For summarise, full list of abrije regexes:")
    pi_pp.pprint(the_regexes)

# summarise all log files using abrije
rule summarise_logs:
    input:
        rules.run_all.output
    output:
        summary_out_templater.use()
    params:
        globs = " ".join(the_globs),
        regexes = " ".join(the_regexes)
    shell:
        "abrije "
        "--glob {params.globs} "
        "--regex {params.regexes} "
        "-o {output}"

# for debugging
if verbose: print_all_rules()
