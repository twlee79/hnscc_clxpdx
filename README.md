HNSCC PDX/CLX processing pipeline
---------------------------------

Author: Tet-Woo Lee <tw.lee AT auckland.ac.nz>

Revision history:

* v0.1 2018-09-16
  - Does fastqc&trim
* v0.11 2018-09-18
  - hisat2 align to hg38 and mm10 genomes
* v0.2 2018-09-19
  - disambiguate alignments
* v0.21 2018-09-19
  - trim molecular labels, improve logging
* v0.22 2018-09-21
  - extract molecular labels with umi_tools
* v0.3 2018-09-25
  - realign disambugated fq files, combine and deduplicate
* v0.31 2018-09-25
  - add featureCounts
* v0.32 2018-09-26
  - alter hisat2/featureCounts to use compatible genome&gtk
* v0.33 2018-10-10
  - add script to prepare genomes, update to use these genomes
* v0.34 2018-10-10
  - add fastq counts after relevant steps, fix umi_tools --paired
* v0.40 2019-01-10
  - move parameters to config file

Notes:

1. To prepare genomes (download, build hisat2 indexes etc.), run `bash scripts/prepare_genomes.sh`.

2. Copy input sequencing data to `in_sequencing_data`. Data can be within subdirectories in this directory if desired.

3. To generate list of sequencing data files for use by this Snakemake workflow, run
`python scripts/generate_seqdata_list.py in_sequencing_data in_sequencing_data/seqdata_list.csv`. This will
run in the provided `envs/py37.yaml` conda environment.
